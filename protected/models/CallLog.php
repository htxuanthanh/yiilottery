<?php

/**
 * This is the model class for table "edf_call_log".
 *
 * The followings are the available columns in table 'edf_call_log':
 * @property integer $id
 * @property integer $lead_id
 * @property integer $direction
 * @property integer $status
 * @property integer $created_by
 * @property string $notes
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property string $data
 * @property integer $suspect_id
 * @property string $file_url
 * @property integer $talk_time
 * @property integer $wait_time
 * @property string $call_session
 * @property string $number_phone
 * @property string $extension
 */
class CallLog extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'edf_call_log';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('lead_id, direction, status, created_by, suspect_id, talk_time, wait_time', 'numerical', 'integerOnly'=>true),
			array('file_url, call_session, number_phone, extension', 'length', 'max'=>255),
			array('notes, created_at, updated_at, deleted_at, data', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, lead_id, direction, status, created_by, notes, created_at, updated_at, deleted_at, data, suspect_id, file_url, talk_time, wait_time, call_session, number_phone, extension', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'lead_id' => 'Lead',
			'direction' => 'Direction',
			'status' => 'Status',
			'created_by' => 'Created By',
			'notes' => 'Notes',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'deleted_at' => 'Deleted At',
			'data' => 'Data',
			'suspect_id' => 'Suspect',
			'file_url' => 'File Url',
			'talk_time' => 'Talk Time',
			'wait_time' => 'Wait Time',
			'call_session' => 'Call Session',
			'number_phone' => 'Number Phone',
			'extension' => 'Extension',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('lead_id',$this->lead_id);
		$criteria->compare('direction',$this->direction);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('notes',$this->notes,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);
		$criteria->compare('deleted_at',$this->deleted_at,true);
		$criteria->compare('data',$this->data,true);
		$criteria->compare('suspect_id',$this->suspect_id);
		$criteria->compare('file_url',$this->file_url,true);
		$criteria->compare('talk_time',$this->talk_time);
		$criteria->compare('wait_time',$this->wait_time);
		$criteria->compare('call_session',$this->call_session,true);
		$criteria->compare('number_phone',$this->number_phone,true);
		$criteria->compare('extension',$this->extension,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CallLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
